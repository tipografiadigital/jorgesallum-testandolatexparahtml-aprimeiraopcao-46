\chapter{Título do texto}

\subsection{}

\chapter{Introdução}

Sigmund Freud é figura incontornável do pensamento contemporâneo
ocidental. Sua obra instaura, junto com a de Karl Marx, as duas
discursividades\href{\#fn1}{1} que determinam a nossa compreensão do
mundo, e que estabelecem a sexualidade e o capital como instituintes das
lógicas do prazer e do poder, lógicas que ordenam as relações entre os
homens.

E isso, certamente, não é pouco.

A obra de Freud ganhou corpo e figura na criação cultural de nosso
tempo, se imiscuindo e orientando a produção na arte --- na literatura,
no teatro e no cinema, evidentemente, mas também nas artes plásticas
---, na filosofia, nas ciências humanas e biológicas. Fez``-se presente
em nosso cotidiano miúdo, dando novas significações para o nosso corpo
--- um corpo não só orgânico, mas principalmente erógeno e simbólico
---, para nossa história e nossa infância, tanto a do indivíduo como a
da civilização, para nosso pensar e para nossa percepção, sempre
configurados pela fantasia, para nossa memória, uma memória ativa e
criadora e não mais um mero depósito de lembranças, para nossos
movimentos, ações e afetos, plenos de significações, para nossos desejos
e sonhos.~A psicanálise funda uma subjetividade linguageira inédita, que
questiona a supremacia da frágil razão, que desloca a centralidade ciosa
do Eu, na afirmação do domínio do inconsciente, no vínculo que
estabelece entre sanidade e loucura. Ela transforma nosso presente,
assim como nosso passado e nosso futuro, pelo estabelecimento de uma
causalidade retroativa, em que o futuro refaz o passado, e que permite,
por meio da palavra e da narrativa de nossa história, a reapropriação
singular e inaugural do sentido de nossa própria existência.
Apresenta''-se ainda como uma terapêutica com a potência de desarmar
sintomas penosos pelo trabalho de testemunho do processo de desalienação
que suscita, na admissão de nossos desejos, dos mais sublimes aos mais
abjetos, no enfrentamento de nossos conflitos, com os outros e também
com nós mesmos, sem, no entanto, adotar a impostura covarde frente à dor
do humano, tão ao gosto do capitalismo sôfrego, pela invenção da doença
e pela venda de supostos caminhos facilitados para a completude e a
felicidade, atalhos que apenas nos anestesiam e descolorem a vida. Na
contramão da pretensão alienante, Freud, com sua psicanálise, nos
impeliu e nos impele a que nos aprimoremos na administração do
desamparo, que nos constitui e que é a marca trágica do humano.

Esse campo do humano inaugurado pela psicanálise encontra reciprocidades
no campo estabelecido pela literatura, áreas de produção de conhecimento
que se alicerçam na potência de criação de realidades que a linguagem
proporciona.

``A linguagem'', como escreve o filósofo francês Maurice Merleau-Ponty,

\begin{quote}
é, pois, este aparelho singular que, como nosso corpo, nos dá mais do
que pusemos nela, seja porque apreendemos nossos próprios pensamentos
quando falamos, seja porque os apreendemos quando escutamos outros.
Quando escuto ou leio, as palavras não vêm sempre tocar significações
preexistentes em mim. Têm o poder de lançar"-me fora de meus
pensamentos, criam no meu universo privado cesuras onde outros
pensamentos podem irromper.\href{\#fn2}{2}

\end{quote}
\section{Psicanálise, arte e literatura}

A relação entre psicanálise e arte --- a literatura em particular --- é
mais íntima, mas, também, mais conflituosa do que se poderia imaginar
num primeiro olhar.

Ela tem uma história tão longa quanto a da própria disciplina
psicanalítica e é marcada, ainda hoje, pelo diálogo tempestuoso que
Sigmund Freud estabeleceu com as artes e com os artistas.

Freud, desde os escritos inaugurais da psicanálise, apoiou``-se em
produções artísticas, especialmente em imagens literárias, a fim de dar
corpo e forma às suas próprias criações. São fundamentais as presenças
das obras e das figuras de Sófocles, Shakespeare, Cervantes, Goethe e
Schelling como disparadoras de diálogos fecundos e, até mesmo, como
pilares importantes da aventura psicanalítica. Para qualquer estudioso
da psicanálise é no mínimo intrigante a grande quantidade de citações e
de correspondências que ligam a obra freudiana e seu criador à criação
artística e aos próprios poetas de seu tempo: Mann, Schnitzler, Rolland,
Zweig, entre outros, foram tomados por Freud como interlocutores
privilegiados. Não deve, também, passar despercebido o fato de inúmeros
comentadores terem realçado as qualidades literárias da escrita
freudiana, comparando''-a à dos melhores escritores de língua
alemã.\href{\#fn3}{3} Nesse sentido, não se trata de mera curiosidade o
fato de que o único prêmio oficial recebido em vida por Freud tenha sido
o Prêmio Goethe, na cidade de Frankfurt, em 1930, honraria que lhe foi
concedida como escritor e cientista ``em igual medida''.\href{\#fn4}{4}
Chama, também, a atenção, o grande número de artigos escritos por Freud
que, direta ou indiretamente, tomam uma obra de arte ou a vida de um
artista, como tema a ser desenvolvido para que se avance na formulação
de algum conceito psicanalítico.\href{\#fn5}{5} Ou seja, são muitos os
fios que podem ser puxados desse tecido que liga Freud aos artistas e a
suas produções.

A experiência estética e a criação literária formaram, no decorrer de
toda a sua obra, um pano de fundo com o qual ele debateu, quer para se
aliar à experiência e à criação artísticas, quando estas lhe permitiram
defender sua própria teoria, quer para se contrapor a elas, quando Freud
lhes concede um papel de antagonistas da verdade psicanalítica, por
apenas adocicarem a vida, afastando e alienando os homens de seus reais
conflitos.

Em 15 de outubro de 1897, em carta enviada a seu amigo"-confidente
Wilhelm Fliess, Freud relata de que maneira, em seu caminho de
autoanálise, sustentado no ato mesmo de escrever, os diálogos com o mito
de \emph{Édipo Rei} e com a tragédia \emph{Hamlet} de Shakespeare o
levaram a uma revelação inédita sobre si mesmo e sobre o universo dos
homens.

\begin{quote}
Ocorreu``-me somente uma ideia de valor geral. Também em mim comprovei o
amor pela mãe e os ciúmes contra o pai, a ponto de considerá''-los agora
como um fenômeno geral da infância mais precoce {[}\ldots{}{]}. Se as
coisas se dão mesmo desta maneira, é possível compreender perfeitamente
o apaixonante arrebatamento que \emph{Édipo Rei} causa, apesar de todas
as objeções racionais contra a ideia de um destino inexorável que o
assunto pressupõe: {[}\ldots{}{]} o mito grego retoma uma compulsão de
destino que todos respeitamos porque percebemos sua existência em nós
mesmos. Cada um dos espectadores foi uma vez, em germe e em fantasia, um
Édipo semelhante e, ante a realização onírica deslocada para a
realidade, todos retrocedemos horrorizados, dominados pelo impacto de
toda a repressão que separa nosso estado infantil de nosso estado atual.
Ocorreu"-me fugazmente que este também poderia ser o fundamento de
\emph{Hamlet}. Não me refiro às intenções conscientes de Shakespeare;
prefiro supor que foi um acontecimento real que o impulsionou à
apresentação do tema, uma vez que seu próprio inconsciente compreendia o
inconsciente de seu protagonista. Como explicaria o histérico Hamlet a
sua frase: ``Assim a consciência nos faz todos covardes''? Como
explicaria sua vacilação em matar o tio para vingar o pai, quando ele
mesmo não teve maior constrangimento em mandar seus cortesãos para a
morte e em assassinar Laerte tão prontamente? Como explicar tudo isto
senão pelo tormento que nele desperta a obscura recordação de que ele
mesmo meditou sobre um crime idêntico contra o pai, impulsionado por sua
paixão pela mãe?\href{\#fn6}{6}

\end{quote}
Esta passagem evidencia a potência heurística, inventiva e inspiradora
da literatura para a criação dos conceitos nucleares da psicanálise: foi
no e pelo diálogo com as produções literárias que Freud pôde conceber e
dar forma para as suas criações conceituais. Mas não só: foi também no e
pelo diálogo com as produções literárias que o homem Sigmund Freud
construiu sua própria subjetividade assim como a subjetividade
linguageira --- esta, feita de linguagem --- de todos nós. Foi,
portanto, por meio do encontro instituinte com a literatura que Freud
foi capaz de --- ao admitir seu desejo incestuoso pela mãe e seus
sentimentos ambivalentes pelo pai --- afiançar e universalizar seus
achados.

E assim, alinhado à vanguarda de seu tempo e assumindo uma compreensão
inaugural com relação à função e à potência da linguagem, Freud escreve,
ainda antes do texto citado, em 1890:

\begin{quote}
{[}{]} as palavras são efetivamente a ferramenta essencial do tratamento
psíquico.~O leigo considerará, talvez, dificilmente concebível que os
distúrbios mórbidos do corpo ou da alma possam ser dissipados pela
``simples'' fala do médico, pensará que lhes estão pedindo para crer na
magia. No que ele não estará inteiramente errado; as palavras dos nossos
discursos cotidianos é que não passam de magia
descolorida.\href{\#fn7}{7}

\end{quote}
Podemos retirar deste trecho algo ainda mais fundamental: foi esta mesma
magia que Freud ouviu na narrativa de suas pacientes histéricas e que o
endereçou em definitivo para o reino encantado das palavras, da
linguagem. Linguagem que se mostra com toda a sua potência criadora,
criadora de uma nova subjetividade; linguagem que é palavra poética e
não apenas veículo de comunicação de ideias ou de transmissão de um
conhecimento já dado, tal como a compreendiam, até então, a ciência e a
filosofia. Tal palavra não é mais descritiva e acessória; ela se torna
\emph{literatura}, oportunidade de criação de sentidos e de
realidades.\href{\#fn8}{8}

É o pensador francês Roland Barthes que, em \emph{Aula}, nos situa em
relação à força geradora de saber da literatura:

\begin{quote}
Essa trapaça salutar, essa esquiva, esse logro magnífico que permite
ouvir a língua fora do poder, no esplendor de uma revolução permanente
da linguagem, eu a chamo, quanto a mim: literatura. {[}\ldots{}{]} A
ciência é grosseira, a vida é sutil, e é para corrigir essa distância
que a literatura nos importa. Por outro lado, o saber que ela mobiliza
nunca é inteiro nem derradeiro; a literatura não diz que sabe alguma
coisa, mas que sabe de alguma coisa; ou melhor: que ela sabe algo das
coisas --- que sabe muito sobre os homens.\href{\#fn9}{9}

\end{quote}
É esta a linguagem que Freud abraçou: linguagem poética que ``cura''
quando se oferece plena, como literatura, na experiência de reescritura
de cada história particular.

Imerso no simbólico, este novo homem, recém"-nascido, tem sua fundação
na linguagem. Este é o homem de Freud: o homem psicanalítico, o homem
simbólico.

``A linguagem'', escreveu Maurice Merleau-Ponty,

\begin{quote}
não é mais a seiva das significações, mas é o próprio ato de significar,
e o homem falante ou o escritor não pode governá``-la voluntariamente,
assim como o homem vivente não pode premeditar o detalhe e os meios dos
seus gestos.~A única maneira para compreender a linguagem é
instalar''-se nela e exercê"-la.\href{\#fn10}{10}

\end{quote}
E prossegue:

\begin{quote}
O que há de risco na comunicação literária, e de ambíguo, irredutível à
tese em todas as grandes obras de arte não é um delíquio provisório do
qual se pudesse esperar eximi"-la, mas o esforço a que se tem de
consentir para atingir a literatura, ou seja, a \emph{linguagem a
explorar}, que nos conduz a perspectivas inéditas em vez de nos
confirmar as nossas.\href{\#fn11}{11}

\end{quote}
Mas em um de seus primeiros textos psicanalíticos que vieram a público,
\emph{Estudos sobre a histeria} (1893--1895), Freud deixa bem clara a
ambiguidade --- uma urgência de proximidade, mas também de distância ---
que regula sua relação com essa ``linguagem a explorar'', a literatura
de que fala Merleau-Ponty:

\begin{quote}
A mim causa singular impressão comprovar que minhas histórias clínicas
carecem, por assim dizer, do severo selo da ciência, e que apresentam
mais um caráter literário. Mas consolo"-me pensando que este resultado
depende inteiramente da natureza do objeto, e não de minhas preferências
pessoais.~O diagnóstico local e as reações elétricas não têm eficácia
alguma na histeria, enquanto uma exposição detalhada dos processos
psíquicos, tal como estamos habituados a encontrar na literatura, me
permite chegar, por meio de um número limitado de fórmulas psicológicas,
a um certo conhecimento da origem de uma histeria.\href{\#fn12}{12}

\end{quote}
À falta do selo de garantia oferecido pela ciência, Freud buscou consolo
e abrigo na atividade literária, no poder criador da linguagem. Mas,
certamente, não sem um grande desconforto.~E a partir da constatação
desse grande mal``-estar, observamos que se, num primeiro instante, a
literatura pôde ser o solo germinal e a fiadora universal para a criação
freudiana, num contramovimento, numa traição ao pacto criador original,
ela foi deitada, à sua revelia, no divã psicanalítico, para que dela,
então, se extraíssem --- depois do projeto psicanalítico tê''-las ali
colocado --- as confirmações necessárias para a manutenção das hipóteses
metapsicológicas elaboradas.

A criatura, a psicanálise, virou"-se contra o impulso criador.

E, assim, se a obra literária teve o papel de musa inspiradora, de
cúmplice, para a criação freudiana, por um revés, ela verá sua trama
cadaverizada, dissecada para a confirmação das premissas que, no momento
inicial, foram por ela mesma despertadas.~É no interior dessa vertente
conquistadora da relação da psicanálise com a literatura que proliferam
estudos que reduzem a obra literária a um mero sintoma da neurose de seu
autor, numa subtração de seu valor original de saber norteador para a
própria criação da teoria psicanalítica.

O que se evidencia, assim, nessa segunda modalidade da relação com a
arte, é a negação das bases que forneceram as condições de possibilidade
para a criação dessa nova formulação do humano que a psicanálise é.

E, da mesma forma, se é evidente um olhar e um uso ambivalentes de Freud
em sua parceria com a obra de arte, não é de se estranhar que, ao
historiar sua produção, seja possível verificar também, não uma posição
unitária, mas, bem ao contrário, uma atitude de extrema ambiguidade em
sua ligação com a figura do \emph{artista}.

Dessa maneira, ora o artista deve ocupar a posição de cúmplice, daquele
que antecipa suas próprias conclusões relativas à alma humana,
avalizando"-o, ora é levado a ocupar o papel de rival, do ser
dissimulado, do falsificador insidioso, do grande mentiroso que mitiga
as relações com a realidade por meio de fórmulas suaves, conquistando
popularidade, coisa que o psicanalista não é capaz de alcançar com suas
teorias.

Ciumento, Freud declara: ``O artista conquista por meio de sua fantasia
o que antes não existia a não ser em sua fantasia: honras, poder e o
amor das mulheres'',\href{\#fn13}{13} e se queixa:

\begin{quote}
E se pode bem dar um suspiro, quando nos damos conta de que é assim
concedido a certos homens fazer surgir, do torvelinho de seus próprios
sentimentos, verdadeiramente sem esforço algum, os mais profundos
conhecimentos, enquanto nós outros, para atingi"-los, devemos abrir
caminho tateando sem cessar em meio às mais cruéis
incertezas.\href{\#fn14}{14}

\end{quote}
E assim, ao destacar o dilema freudiano frente às relações da
psicanálise com a produção artística e com a figura do artista,
existente desde seus primeiros textos até suas últimas obras, é possível
colocar em relevo uma questão epistemológica fundamental e que diz
respeito à concepção de construção de conhecimento que orienta Freud na
montagem da disciplina que ele inaugura. Freud, em sua dupla navegação,
estaca na encruzilhada arte/ciência, uma encruzilhada que ele mesmo se
impõe.

A psicanálise, diante da oposição desvelamento/criação, oposição que
abriga uma ruptura entre proposições contrastadas frente à questão da
criação do saber, optaria por qual destes percursos? O psicanalista
seria aquele que desvenda e assim revela um conhecimento preexistente,
esquecido e soterrado pelos escombros da repressão, agindo como um
arqueólogo --- metáfora largamente utilizada no decorrer da obra
freudiana para revelar o trabalho psicanalítico ---, um cientista que
explora a Natureza a fim de identificar as leis gerais que a regem, ou,
em contrapartida, atuaria como o artista, figura que promove tamanho
temor e estranhamento em Freud, que optaria pela via da criação, com o
emprego de sua fantasia, de realidades sempre inaugurais?

Compreende``-se, então, que a ambiguidade que regula a relação de Freud
com o escritor e com sua obra, e que o faz colocá''-los numa posição
ambivalente, ora de cúmplices, ora de rivais, é a evidência do extremo
mal"-estar experimentado por ele mesmo, justamente por não poder deixar
de assumir a via criadora como instauradora de sua psicanálise. Pois,
partindo do amparo permitido pelo campo estético, Freud quis, ao renegar
o seu passado, conquistar para sua disciplina uma condição puramente
científica.

É o pintor suíço Paul Klee quem nos auxilia a superar a ambivalência que
viemos ressaltando até agora: ``A arte não reproduz o visível, faz
visível''.\href{\#fn15}{15} A relação psicanalítica pode se pautar nesta
mesma visão, ao se comprometer com a fantasia, numa estética própria,
questionando uma linhagem cientificista que se afiança na crença de
desocultamento de leis gerais atemporais e a nós destinadas.~O fazer
psicanalítico pode ser então assumido em sua responsabilidade criadora,
respondendo por seus próprios atos, retomando sua função intrínseca, que
é a de dar existência a algo que não teria vida sem seu gesto de
criação.

\section{Os textos escolhidos}

Os textos escolhidos neste livro fazem parte daquilo que se convencionou
denominar ``psicanálise aplicada'', ou seja, o emprego das teorias
psicanalíticas para além do campo médico e psicopatológico. Tal
aplicação especulativa da psicanálise é, na avaliação de Freud, ``o
esforço da ciência da alma humana no sentido de levar a uma compreensão
profunda da vida humana'' em todos os seus âmbitos, das situações
cotidianas mais simples e que revelam sentidos ignorados, tais como
lapsos de linguagem, esquecimentos, atos sintomáticos, chistes e sonhos,
aos grandes campos de produção de conhecimento, sejam eles as artes, a
mitologia, a história das religiões e civilizações, a antropologia, a
educação, a sociologia e a política.

A primeira obra resultante desse esforço de expansão da psicanálise foi
o texto de Freud, \emph{Delírios e sonhos na Gradiva de Jensen},
publicado em 1907, na coletânea \emph{Schriften zur angewandten
seelenkunde} (\emph{Monografias de psicanálise aplicada}), coleção
criada especialmente para abrigar ensaios de psicanálise aplicada, de
Freud e de seus novos discípulos, médicos em sua maioria, mas também
intelectuais atuantes nas artes e nas humanidades, e que formaram, entre
os anos de 1902 e 1907, a Sociedade Psicológica das Quartas-Feiras,
primeira instituição psicanalítica. Nesse texto, Freud defende, de um
lado, a existência de um parentesco entre os processos inconscientes e a
atividade criadora, uma cumplicidade entre a psicanálise e a arte, mas
de outro, realiza um exercício de sobreposição dos conceitos
psicanalíticos à obra literária, uma demonstração do valor universal da
psicanálise que acaba por desautorizar a inventividade da criação
artística, restringindo sua engenhosidade ao mero saldo dos conflitos
vividos na infância pelo autor.

Tal atitude ambivalente, de parceria e rivalidade, marcou, como vimos, a
relação de Freud com as obras artísticas e seus autores durante toda sua
vida, e poderá ser também reconhecida nas obras selecionadas. Mas vale a
pena realçar aqui as ampliações conceituais importantes que foram
estabelecidas justamente no diálogo fertilizador com as obras
literárias.

Em ``O poeta e o fantasiar'' (1908 {[}1907{]}), Freud apresenta sua
teoria geral sobre a criação artística, vinculando``-a às fantasias
erógenas e ambiciosas, material propulsor para as experiências e desejos
da criança e que seguem atuantes na vida adulta. Toma como pressuposto
que ``a poesia, assim como o sonho de vigília, vem a ser uma continuação
e um substituto da brincadeira infantil de outrora''.\href{\#fn16}{16}
Acrescenta que ``toda criança que brinca se porta como um poeta, uma vez
que ela cria para si o seu próprio mundo, ou, para dizer com mais
precisão, transpõe as coisas de seu mundo para uma nova ordem, que lhe
agrada. {[}\ldots{}{]}.~O poeta faz o mesmo que a criança que brinca:
cria um mundo de fantasia e o leva muito a sério; isto é, ele o provê de
grande investimento afetivo, ao mesmo tempo em que nitidamente o separa
da realidade.~E a linguagem conserva esse parentesco entre as
brincadeiras em que a criança atua e o produzir poesia
{[}\ldots{}{]}''.\href{\#fn17}{17} As experiências da tenra infância
povoam a fantasia do adulto e configuram tanto o lugar psíquico que ele
ocupa como o modo de relação que ele é capaz de estabelecer junto a
outros.

Em ``Romance familiar do neurótico'' (1909 {[}1908{]}) Freud pesquisa
novos aspectos do Complexo de Édipo ao destacar os romances familiares
que a criança constrói na relação com seus pais; se, de início, os pais
são as figuras principais de investimento amoroso e também de ódio,
autoridades todo``-poderosas para seus filhos, aos poucos, devem ser
criticadas e substituídas, no processo de amadurecimento e autonomia a
ser conquistado na idade adulta, pelo próprio indivíduo e por novos
objetos de investimento: ``o desenvolvimento da sociedade reside
sobretudo nessa oposição entre ambas as gerações''.\href{\#fn18}{18} A
análise que Freud faz da novela familiar dos neuróticos revela a posição
psíquica que o indivíduo ocupa em sua configuração familiar, as
fantasias eróticas e de ambição, subjacentes a todas as relações
humanas, e as dificuldades na progressão do desenvolvimento emocional.

``Uma lembrança infantil de Poesia e verdade'' (1917) é um pequeno
ensaio sobre uma observação apresentada por Goethe no início de sua
autobiografia. Nele, Freud se pergunta sobre o porquê de Goethe, aos
sessenta anos de idade, ter guardado e apresentado determinada
experiência vivida quando não tinha mais do que quatro anos: o escritor
retoma a prazerosa experiência de, sob o olhar divertido dos vizinhos,
lançar para fora da janela de sua casa várias peças de louça.

Freud questiona a veracidade dessa lembrança e pondera sobre seu
possível significado: ``Muito mais seria o caso de supor que o que a
memória conservou seria também o mais significativo de toda essa etapa
da vida, podendo ou já deter tal importância àquela época ou tê''-la
adquirido depois, pela influência de vivências posteriores.
{[}\ldots{}{]} Para reconhecê``-las em sua significância, faz''-se
necessário algum trabalho de interpretação, que ou comprova como o seu
conteúdo é substituído por outro, ou indica a sua relação com outras
vivências importantes e irreconhecíveis para as quais as chamadas
lembranças encobridoras eram substitutos''.\href{\#fn19}{19}

Reflete, portanto, sobre a atividade exercida por nossa memória, o
processo de seleção e avaliação que ela realiza sobre aquilo que passa a
ter, ou não, valor para nossa consciência. Essa memória viva torna"-se,
justamente por esta sua função seletiva, uma memória criadora --- e não
mais um depósito inanimado de imagens ---, uma memória que significa
nossa história e que permite a criação de novas narrativas, que resultam
justamente desse processo de edição em parceria com o recalque, nas
quais as experiências vividas em nossa infância ganham ou perdem
relevância. Temos, assim, estabelecida uma causalidade retroativa, na
qual o futuro é capaz de dar novas significações para nosso passado.

Em ``O estranho'' (1919), Freud dedica"-se à investigação do
\emph{Unheimlich}, uma sensação de estranha familiaridade, uma impressão
assustadora, que pertence ao que causa medo, ao que suscita horror:
``{[}\ldots{}{]} esse estranho não é realmente nada novo ou alheio, e
sim algo de há muito familiar à vida anímica, que a ela só se tornou
estranho pelo processo do recalque.~A ligação com o recalque nos é agora
elucidada também pela definição de Schelling, segundo a qual o estranho
seria algo que deveria permanecer oculto, e no entanto ele
aflora''.\href{\#fn20}{20} ``O estranho é, pois, {[}\ldots{}{]} algo que
outrora foi familiar e de há muito conhecido. {[}\ldots{}{]} o familiar
enraizado, que experimentou um recalque e dele retorna
{[}\ldots{}{]}.\href{\#fn21}{21} Tal impressão de estranheza aparece no
dia a dia e também na criação estética e, tanto num caso como no outro,
é o resultado do retorno de certos complexos infantis que teriam sido
superados ou recalcados. Surge na presença de certos temas que remetem
ao temor à castração, às figuras do duplo e dos autômatos, que reativam
forças primitivas que a cultura pareceria ter esquecido e o indivíduo
supunha ter superado. Mais uma vez Freud salienta a pregnância das
experiências dos primeiros anos de vida, que se fazem presentes também
na vida madura.

``Dostoiévski e o parricídio'' (1928 {[}1927{]}) é um exercício de
psicobiografia. Freud jamais desdenha das capacidades literárias de
Dostoiévski, muito ao contrário, o pareia aos grandes escritores,
afiança que seu dom artístico, como o de qualquer outro artista, é
inanalisável,\href{\#fn22}{22} mas o condena em sua falta ética,
resultado da onipresença de sua neurose: ``{[}\ldots{}{]} a pulsão
destrutiva extremamente forte de Dostoiévski, que facilmente o teria
feito criminoso, foi em vida orientada sobretudo contra a sua própria
pessoa (para dentro, em vez de para fora), vindo a se expressar como
masoquismo e sentimento de culpa''.\href{\#fn23}{23}

Quanto aos sintomas de epilepsia, Freud os relaciona ao efeito de
identificação que Fiódor teria experimentado quando seu pai foi
assassinado pelos colonos de sua propriedade rural: ``Conhecemos o
sentido e a intenção de tais ataques que se assemelham à morte. Eles
significam uma identificação com um morto, com uma pessoa, que realmente
está morta, ou que está viva e a ela se deseja a morte.~O último caso é
o mais significativo: o ataque tem o valor de uma punição. Desejou''-se
a morte de outro, e agora é"-se esse outro e se está morto. Aqui a
teoria psicanalítica introduz a asserção de que esse outro, para o
garoto, via de regra é o pai, e o ataque {[}\ldots{}{]} é uma
autopunição para o desejo de morte contra o odiado pai. Segundo
concepção conhecida {[}Freud em \emph{Totem e Tabu}{]}, o crime
principal e primordial, da humanidade como do indivíduo, é o
parricídio''.\href{\#fn24}{24}

Assim como Sófocles, com seu herói Édipo, que simboliza o desejo
parricida universal inconsciente e que se disfarça de destino, e
Shakespeare, com seu Hamlet, que na sua ação assassina indireta
configura a subjetividade contemporânea culpada e inibida em seus atos,
Dostoiévski, com seus três irmãos Karamázov, põe em cena, e sem
máscaras, a própria pulsão assassina em ato, a universalidade do desejo
parricida, a universalidade do Complexo de Édipo.\href{\#fn25}{25} É
surpreendente a perspicácia de Freud: de um só golpe desvenda a mudança
de mentalidade da humanidade e cria uma nova subjetividade.~E tudo isto
por meio da análise de textos literários.

Mas há, no exercício psicopatobiográfico que Freud faz de Fiódor
Dostoiévski, uma condenação moral ao próprio autor: ``É indiferente quem
efetivamente cometeu o ato, e para a psicologia importa apenas quem o
desejou em sentimento e, quando aconteceu, saudou''-o com as
boas``-vindas. {[}\ldots{}{]} A simpatia de Dostoiévski pelo criminoso
de fato desconhece limites, ultrapassando em muito a compaixão à qual o
infeliz tem direito {[}\ldots{}{]}.~O criminoso é para ele quase que um
redentor, que toma a culpa para si, sem o qual ela teria de ser levada
pelos outros. Isso é não só compaixão benéfica: é também identificação
com base no mesmo impulso assassino, e na verdade um e o mesmo
narcisismo ligeiramente deslocado''.\href{\#fn26}{26}

Por fim, vale o convite à leitura dos textos de Sigmund Freud. Seu texto
é rico e engenhoso, agradável e inquietante; a argumentação é clara e
contundente, e ele sabe conduzir seus leitores com mãos de mestre. Esta
coletânea traz uma boa amostra de seu talento de escritor.

\begin{enumerate}
\item
  M. Foucault, \emph{O que é um autor?}. Lisboa: Passagens/Vega, 2002.
  Nesta obra, Foucault discute o conceito de ``instauradores de
  discursividade'', pensadores que não são apenas autores de suas obras
  e de seus livros, mas, autores, como Freud e Marx, que ``produziram a
  possibilidade e a regra de formação de outros textos''. In: M.
  Foucault, ``Qu'est''-ce qu'um auteur?'', \emph{Bulletin de la Société
  Française de Philosophie}, 63e. année, n.3, julho"-setembro de 1969,
  pp. 73--95.\href{\#fnref1}{↩}
\item
  Merleau-Ponty, M. \emph{Signes}. Paris: Gallimard,
  1960.\href{\#fnref2}{↩}
\item
  Ver, a este respeito, por exemplo, A. Döblin, ``Zum siebzigsten
  Geburtstag Sigmund Freud'' (``No septuagésimo aniversário de Sigmund
  Freud''). In: \emph{Almanach für das Jahr 1927}; P. Gay, ``Sigmund
  Freud: um alemão e seus dissabores''. In: \emph{Sigmund Freud e o
  gabinete do doutor Lacan}, P. C. Souza (org.). São Paulo: Brasiliense,
  1990; W. Muschg, ``Freud escritor''. In: \emph{La Psychanalyse}, vol.
  V, reimpressão em \emph{Freud. Jugements et témoignages}, R. Jaccard
  (org.). Paris: puf, 1976; T. Mann, ``Freud et l'avenir''. In:
  \emph{Freud. Jugements et témoignages}, op. cit.; T. Jones, \emph{A
  vida e a obra de Sigmund Freud}. Rio de Janeiro: Imago, 1989, 3
  volumes; P. Mahony, \emph{On Defining Freud's Discourse}. New
  Haven/London: Yale University Press, 1989; L. Flem, \emph{O homem
  Freud.~O romance do inconsciente}. Rio de Janeiro: Campus, 1993, entre
  outros.\href{\#fnref3}{↩}
\item
  De Dr. Alfons Paquet, Secretário do Comitê do Prêmio Goethe em 26 de
  julho de 1930. Citado por P. Gay em ``Sigmund Freud: um alemão e seus
  dissabores''. In: \emph{Sigmund Freud e o gabinete do doutor Lacan},
  op. cit., p. 23.\href{\#fnref4}{↩}
\item
  Não é o caso aqui de nos estendermos na relação de Freud com a arte e
  suas reflexões a respeito desta complexa questão. Muitos trabalhos
  tratam deste tema com a abrangência necessária. Mas é necessário
  apontar para o fato de que podemos encontrar, recorrentemente, na obra
  freudiana --- desde seus inícios até seus trabalhos mais tardios ---,
  sua curiosidade e necessidade de entendimento do valor da arte e do
  trabalho do artista. Já em \emph{Estudos sobre a histeria}
  (1893--1895), Freud reconhece, em seu ofício, laços com a atividade
  poética. Em a \emph{Interpretação dos sonhos} (1900), o psicanalista
  legitima a universalidade do que passaria a ser denominado de Complexo
  de Édipo através de suas análises das tragédias gregas e da obra de
  Shakespeare. Em \emph{Os chistes e sua relação com o inconsciente}
  (1905),~ ``Escritores criativos e devaneio''~
  (1908{[}1907{]}) e ``O estranho'' (1919), o tema da arte e o do
  trabalho do artista são centrais. Freud realiza, também, trabalhos
  mais específicos sobre alguns artistas, \emph{Delírios e sonhos na
  `Gradiva' de Jensen} (1907), \emph{Uma lembrança infantil de Leonardo
  da Vinci} (1910), ``Moisés de Michelangelo'' (1914), ``Dostoiévski e o
  parricídio'' (1928).~O tema da arte é, também, retomado em textos de
  caráter mais geral como em \emph{O futuro de uma ilusão} (1927) e
  \emph{O mal"-estar na civilização} (1930{[}1929{]}), bem como em
  artigos claramente vinculados à clínica psicanalítica, como ``Terapia
  analítica'', conferência xxviii, e ``O caminho da formação dos
  sintomas'', conferência xxiii, ambas pertencentes às
  \emph{Conferências introdutórias sobre Psicanálise} (1916--1917).
  Outros tantos trabalhos de Freud são permeados por sua interlocução
  com a arte e a figura do artista; é possível encontrar, na tradução
  brasileira das obras de Freud, um apêndice que arrola suas obras que
  tratam da arte ou da estética como motivo central (vol. xxi, p. 247);
  só aí são vinte e dois textos. No índice de ``Obras de arte e
  literatura'' (vol. xxiv, pp. 91--100), citados no decorrer de toda a
  obra do psicanalista vienense, temos pelo menos quatro centenas de
  menções. Enfim, basta ler a obra de Freud para perceber que a arte e o
  artista estão insistentemente presentes em suas reflexões e que, se em
  muitas ocasiões estas obras e estes artistas lhe foram úteis para lhe
  afiançar suas próprias criações, de outro lado, estes lhe
  representaram também rivais, que já se assentavam em um terreno que
  ele supunha desbravar. Freud oscila entre a cumplicidade total até a
  desconfiança, passando da reverência ao desrespeito frente ao artista,
  a suas obras e à produção de conhecimento nelas implícita.~É
  importante, então, guardar que aquilo que marca a relação de Freud, e
  de sua psicanálise, com a produção artística e com a própria figura
  condensada do artista é uma \emph{estranha
  familiaridade}.\href{\#fnref5}{↩}
\item
  S. Freud, \emph{Los origenes del psicoanalisis.} Madri: Alianza,
  1975.\href{\#fnref6}{↩}
\item
  S. Freud, ``Tratamento psíquico (tratamento da alma)''. In:
  \emph{Resultats, idées, problèmes}, i, pp. 1--23. Citado por L. Flem,
  em \emph{O homem Freud.~O romance do inconsciente}. Rio de Janeiro:
  Campus, 1993, p. 228.\href{\#fnref7}{↩}
\item
  M. Foucault, \emph{As palavras e as coisas} {[}1966{]}. Trad.~A. R.
  Rosa. São Paulo: Martins Fontes, 1967, pp. 393--4. ``É que, no início
  do século xix, na época em que a linguagem se entranhava na sua
  espessura de objeto e se deixava, de parte a parte, atravessar por um
  saber, reconstituía"-se ela alhures, sob uma forma independente, e de
  difícil acesso, dobrada sobre o enigma de seu nascimento e,
  inteiramente, referida ao puro ato de escrever.~A literatura é a
  contestação da filologia (de que ela é, no entanto, figura gêmea): ela
  reconduz a linguagem da gramática ao puro poder de falar, e aí
  encontra o imperioso, \emph{o selvagem ser das palavras}. Da revolta
  romântica contra um discurso imobilizado na sua cerimônia à descoberta
  mallarmiana da palavra no seu poder impotente, vê"-se bem qual foi, no
  século xix, a função da literatura em relação ao moderno modo de ser
  da linguagem. Em relação ao fundo desse jogo essencial, o resto é
  efeito: a literatura distingue``-se cada vez mais do discurso de
  ideias, e fecha''-se numa intransitividade radical; destaca``-se ela
  de todos os valores que podiam na Idade Clássica fazê''-la circular (o
  gosto, o prazer, o natural, o verdadeiro), e faz nascer no seu próprio
  espaço tudo o que possa assegurar a denegação lúdica desses valores (o
  escandaloso, o feio, o impossível); rompe com todas as definições de
  `gêneros' como formas ajustadas a uma ordem de representações e
  torna``-se pura e simples manifestação de uma linguagem que não tem
  por lei senão afirmar --- contra todos os outros discursos --- a sua
  existência abrupta; não lhe resta então senão recurvar''-se num
  perpétuo retorno a si, como se o seu discurso não pudesse ter por
  conteúdo senão o dizer a sua própria forma: dirige``-se a si como
  subjetividade específica do ato de escrever, ou procura apoderar''-se,
  no movimento que a faz nascer, da essência de toda a literatura; e
  assim, todos os seus fios convergem para a ponta mais fina ---
  singular ---, instantânea, e no entanto absolutamente universal ---,
  para o simples ato de escrever. No momento em que a linguagem, como
  palavra disseminada, se torna objeto de conhecimento, eis que
  reaparece sob uma modalidade estritamente oposta: silenciosa,
  cautelosa colocação da palavra sobre a brancura do papel, onde ela não
  pode ter nem a sonoridade nem interlocutor, onde nada mais tem a
  dizer, nada mais a fazer do que cintilar no fulgor de seu próprio
  ser''.\href{\#fnref8}{↩}
\item
  R. Barthes, \emph{Aula. Aula inaugural da cadeira de semiologia
  literária do Colégio de França (pronunciada dia 7 de janeiro de
  1977)}. São Paulo: Cultrix, pp. 16--9.\href{\#fnref9}{↩}
\item
  M. Merleau-Ponty, \emph{Sygnes}, op. cit., p. 23.\href{\#fnref10}{↩}
\item
  M. Merleau-Ponty, ``A linguagem indireta e as vozes do silêncio''
  (1952). In: \emph{Merleau-Ponty}. Coleção \emph{Os pensadores.} São
  Paulo: Abril Cultural, 1980, p. 170.\href{\#fnref11}{↩}
\item
  S. Freud, \emph{Estudos sobre a histeria} (1893--1895). Esta versão
  foi retirada de R. Mezan, \emph{Freud, pensador da cultura}. São
  Paulo: Cnpq/Brasiliense, 1985, p. 607.\href{\#fnref12}{↩}
\item
  S. Freud, ``Os caminhos da formação dos sintomas'', Conferência xxiii
  das \emph{Conferências introdutórias sobre a psicanálise}
  (1916{[}17{]}), Vol. xvi. In: \emph{Obras completas de Sigmund Freud},
  p. 439.\href{\#fnref13}{↩}
\item
  S. Freud, \emph{O mal"-estar na civilização}, Vol. xxi. In:
  \emph{Obras completas de Sigmund Freud}.\href{\#fnref14}{↩}
\item
  Klee, P. ``Schöpferische Konfession''. In: \emph{Tribune der Kunst und
  Zeit}, vol. xiii, 1920. Citado por C. Geelhaar, in \emph{Paul Klee et
  le Bauhaus}. Neuchâtel, Suíça: Editions Ides et Calendes, 1972, p.
  26.\href{\#fnref15}{↩}
\item
  p. 113.\href{\#fnref16}{↩}
\item
  p. 104.\href{\#fnref17}{↩}
\item
  p. 131.\href{\#fnref18}{↩}
\item
  p. 119.\href{\#fnref19}{↩}
\item
  p. 87.\href{\#fnref20}{↩}
\item
  p. 92.\href{\#fnref21}{↩}
\item
  p. 35.\href{\#fnref22}{↩}
\item
  Idem.\href{\#fnref23}{↩}
\item
  p. 41.\href{\#fnref24}{↩}
\item
  pp. 48--9.\href{\#fnref25}{↩}
\item
  p. 50.\href{\#fnref26}{↩}
\end{enumerate}
